from django.forms import ModelForm
from todos.models import TodoList, TodoItem


# make a form with the fields: name
class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
